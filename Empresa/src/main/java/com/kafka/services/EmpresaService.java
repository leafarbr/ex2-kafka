package com.kafka.services;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.kafka.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class EmpresaService {

    @Autowired
    private KafkaTemplate<String, Empresa> producer;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    public void enviarAoKafka(Empresa empresa) {
        producer.send("spec3-rafael-antonio-2", empresa);
    }
}



