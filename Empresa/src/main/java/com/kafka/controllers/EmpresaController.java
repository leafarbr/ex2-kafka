package com.kafka.controllers;

import com.kafka.Empresa;
import com.kafka.services.EmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/empresa")
public class EmpresaController {

    @Autowired
    private EmpresaService empresaService;

    @PostMapping
    Empresa cadastrarEmpresa(@RequestBody Empresa empresa)
    {

        empresaService.enviarAoKafka(empresa);

        return empresa;
    }

}
