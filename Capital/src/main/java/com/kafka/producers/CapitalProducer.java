package com.kafka.producers;

import com.kafka.Empresa;
import com.kafka.EmpresaValida;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class CapitalProducer {

    @Autowired
    KafkaTemplate<String, EmpresaValida> kafkaservice;

    public void enviarEmpresaValidaKakfa(EmpresaValida empresa)
    {
        kafkaservice.send("spec3-rafael-antonio-3", empresa);
    }


}
