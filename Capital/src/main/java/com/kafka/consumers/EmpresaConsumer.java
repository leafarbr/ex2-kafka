package com.kafka.consumers;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.kafka.Empresa;
import com.kafka.EmpresaValida;
import com.kafka.clients.EmpresaGovernoClient;
import com.kafka.producers.CapitalProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class EmpresaConsumer {

    @Autowired
    private EmpresaGovernoClient governoConsumer;

    @Autowired
    private CapitalProducer capitalProducer;

    @KafkaListener(topics = "spec3-rafael-antonio-2", groupId = "valida-empresa-capital")
    public void receber(@Payload Empresa empresa){

        System.out.println("Leu Empresa cadastrada - CNPJ - " + empresa.getCnpj());

        // Implementar Validar na API do Governo
        EmpresaValida ojbEmpresaValida = new EmpresaValida();

        ojbEmpresaValida = governoConsumer.consultarEmpresaPorCNPJ(empresa.getCnpj());

        if (ojbEmpresaValida.getCapital_social() > 1000000) {
            capitalProducer.enviarEmpresaValidaKakfa(ojbEmpresaValida);
        }

    }

}
