package com.kafka.clients;

import feign.Response;
import feign.codec.ErrorDecoder;

public class EmpresaGovernoClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 404) {
            throw new EmpresaGovernoClientException();
        } else {
            return errorDecoder.decode(s, response);
        }
    }




}
