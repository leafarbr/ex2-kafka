package com.kafka.clients;

import com.kafka.EmpresaValida;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@FeignClient(name = "cnpj", url = "https://www.receitaws.com.br/v1/cnpj/", configuration = EmpresaGovernoClientConfiguration.class)
public interface EmpresaGovernoClient {

    @GetMapping("/{cnpj}")
    EmpresaValida consultarEmpresaPorCNPJ(@PathVariable String cnpj);

}

