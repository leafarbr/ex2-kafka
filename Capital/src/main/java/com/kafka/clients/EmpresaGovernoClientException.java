package com.kafka.clients;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@ResponseStatus(code= HttpStatus.BAD_REQUEST, reason="CNPJ não localizado no sistema")
public class EmpresaGovernoClientException extends RuntimeException{
}
