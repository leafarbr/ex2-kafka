package com.kafka.clients;

import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class EmpresaGovernoClientConfiguration {

    private ErrorDecoder errorDecoder = new ErrorDecoder.Default();

    @Bean
    public ErrorDecoder getErrorDecoder() {
        return new EmpresaGovernoClientDecoder();
    }




}
