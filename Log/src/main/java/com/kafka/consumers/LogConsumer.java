package com.kafka.consumers;

import com.kafka.EmpresaValida;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;

@Component
public class LogConsumer {

    @KafkaListener(topics = "spec3-rafael-antonio-1", groupId = "seguranca")
    public void receber(@Payload EmpresaValida empresa) throws IOException {

        System.out.println("Empresa Válida - " + empresa.getCnpj() + " + " + empresa.getNome());

        FileWriter csvFileWriter = new FileWriter("log.csv", true);

        csvFileWriter.append("Empresa Válida - " + empresa.getCnpj() + " + " + empresa.getNome());
        csvFileWriter.append("\n");

        csvFileWriter.flush();
        csvFileWriter.close();

    }
}
